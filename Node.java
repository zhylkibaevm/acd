BABDE

1.public class Main {

   public static void main(String[] args) {
    
      System.out.println("Type: " + stringClean("Type"));
      System.out.println("Take: " + stringClean("Take"));
      System.out.println("Hello: " + stringClean("Hello"));

   }

   public static String Cleanstring(String str) {
      for (int i = 1; i < (str.length()); i++) {
         if (str.charAt(i - 1) == str.charAt(i))
            return Cleanstring(str.substring(0, i - 1) + str.substring(i, str.length()));
      }
         return str;
      }
}

2.public class Main {

   public static void main(String[] args) {
      LinkedList llist = new LinkedList();

      llist.insertAtFirst(5);
      llist.insertAtFirst(6);
      llist.insertAtFirst(7);
      llist.insertAtFirst(8);
      llist.insertAtFirst(9);

      llist.showList();

      llist.removeAtPosition(6);

      llist.showList();
   }
}

3.
public class LinkedList {
    Node head;
    public class Node {
        int value;
        Node next;
        Node(int b) {
            value = b;
            next = null;
        }
    }
    public void FirstAt(int value) {
        Node current = new Node(value);
        current.next = head;
        head = current;
    }


   public void PositionAt(int position) {
        if (head == null)
            return;
        Node temp = head;
        if (position == 0) {
            head = temp.next;
            return;
        }
        for (int i=0; temp!=null && i<position-1; i++)
            temp = temp.next;

        if (temp == null || temp.next == null)
            return;
        Node next = temp.next.next;
        temp.next = next;
    }


    public void Listshow(){
        Node current = head;
        while (current.next != null) {
            System.out.println(current.value);
            current= current.next;
        }
        System.out.println(current.value);
    }
}


2.1.public class Main {

   public static void main(String[] args) {

          Balance balance = new Balance();
      char exp[] = {'{','(',')','}','[',']'};
      if (isBalanced(exp))
         System.out.println("Yes");
      else
         System.out.println("No");
   }

}
2.2 public class Balance {
    public static class stack {
        int index = -1;
        char items[] = new char[100];

        public void push(char x) {
            if (index == 99) {
                System.out.println("Stack is full");
            } else {
                items[index++] = x;
            }
        }

        char pop() {
            if (index == -1) {
                System.out.println("Error");
                return '\0';
            } else {
                char element = items[index];
                index--;
                return element;
            }
        }

        boolean isEmpty() {
            return (index == -1) ? true : false;
        }
    }


    static boolean isMatched(char character1, char character2) {
        if (character1 == '(' && character2 == ')')
            return true;
        else if (character1 == '{' && character2 == '}')
            return true;
        else if (character1 == '[' && character2 == ']')
            return true;
        else
            return false;
    }

    public static boolean isBalanced(char exp[]) {

        stack stack = new stack();


        for (int i = 0; i < exp.length; i++) {
            if (exp[i] == '{' || exp[i] == '(' || exp[i] == '[')
                stack.push(exp[i]);
            if (exp[i] == '}' || exp[i] == ')' || exp[i] == ']') {


                if (stack.isEmpty()) {
                    return false;
                } else if (!isMatched(stack.pop(), exp[i])) {
                    return false;
                }
            }

        }

        if (stack.isEmpty())
            return true;
        else {
            return false;
        }
    }
}

2.3.public class Main {

   public static int Buy(int arr[], int n) {
      int toBuy = arr[0];

      for (int i = 1; i < n; i++)
         toBuy = Math.min(toBuy, arr[i]);
      return toBuy;
   }

   public static int Sell(int arr[], int n) {
      int sell = arr[0];

      for (int i = 1; i < n; i++)
         sell = Math.max(sell, arr[i]);
      return sell;
   }

   public static void main(String[] args) {

      int arr[] = { 10, 7, 5, 8, 11 };
      int n = arr.length;
      System.out.println( "Buy - "+ Buy(arr, n));
      System.out.println( "Sell - " + Sell(arr, n));
   }

}
